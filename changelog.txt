Hot New Features:
#2.1.9 - Emanations should now correctly be circles
#2.1.8 - Removing save actor requirements; this should allow scrolls/wands to work as well as compatibility with TAH.
#2.1.7 - Fix for spell attacks trying to roll strike damage
#2.1.6 - Rolls crit damage if strike crits
#2.1.5 - Enables auto rolling damage for strikes if they hit
#2.1.4 - Made saveDC button logic more resilient, fixed compatibility with latest pf2e system
#2.1.3 - Large refactor, particularly message code
#2.1.2 - Fixed same issue but with saves
#2.1.1 - Fixed issue with players not being able to quick roll attacks
#2.1.0 - Adding in Mortaneus changes and removing more deprecated functions
#2.0.0 - Compatibility with Foundry 0.8.6

Retro Features:
#1.5.1 - Fix for firefox not placing templates correctly
#1.5.0 - Templates are now more lenient about listed areas, multiple templates also supported for spells that have the choice.
	   - Templates are now supported for spells that don't have DCs for whatever reason, or with system saving enabled
	   - Added settings option for releasing targets automatically after spell DCs are rolles
	   - Combined spell save DCs into one message
	   - Fixed some flags in the code
#1.3.3 - Fixed saves after system update, thanks @Ng
#1.3.2 - Added in support to strikes. Thanks for the code @wolfang54
#1.3.1 - Hey There! Removed ALL Instances of Hey There from attack rolls......
#1.3.0 - Any spell with a template programmed in (see spell description if template is missing) now has a button to drag and drop the template onto the map with correct scaling.
#1.3.0 - When rolling group attacks/saves, clicking on items in the chat log will target them in game.
		- double clicking the item selects all of the same success level.
		- shift clicking adds/removes additional selections (works with doubleclick). 
#1.3.0 - Complete rework of the modules structure, using modules now for organisation. Better commenting and lots of refactored code, particularly around hooks and chat listening.
#1.3.0 - Improved the readme significantly, it no longer resembles 1.0.0.

#1.2.0 - Added the option to show bubbles above player heads when rolling.
#1.2.0 - Overhauled the result box visuals.
		- Added Fancy Symbols for success
		- Much clearer distinctions
		- Proper, non-eye-melting color scheme
		- Added More Exclamation Marks!!!!!!
		- Yes.
#1.2.0 - Fixed saves always showing exceed-by value. Thanks for pointing that out Nerull.
#1.2.0 - Refactored message system for more flexibility once again. Probably not done.
#1.2.0 - Fixed wrong version number in intaller.

#1.1.0 - Added option as to whether to send quick-roll results to players.
#1.1.0 - Added option as to whether to show how much a roll exceeded a value by.
#1.1.0 - Added option that controls whether players can use quick rolling at all.
#1.1.0 - Added option to control whether players can use quick-rolling only on their turn.
#1.1.0 - Abstracted chat-messages to be more flexible with future additions.

#1.0.0 - Added Everything.
